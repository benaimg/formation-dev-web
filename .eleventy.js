const pluginNavigation = require("@11ty/eleventy-navigation")

module.exports = function (eleventyConfig) {

  eleventyConfig.addPlugin(pluginNavigation)

  eleventyConfig.addPassthroughCopy('pages/css')
  eleventyConfig.addPassthroughCopy('pages/js')

  return {
    dir: {
      input: "pages"
    },
    pathPrefix: '/~benaimg/'
  }
}
