class Egg {
  constructor (x, y) {
    this.x = x
    this.y = y
  }
}

const WIDTH = 30
const HEIGHT = 20

let canvas = document.querySelector("canvas")
let score = document.querySelector("#score")
let ctx = canvas.getContext("2d")
let eggs = new Set()
let eggsCaught = 0

let x = 200
function handler (e) {
  x = e.offsetX
}
canvas.addEventListener("mousemove", handler)

function frame () {
  const y = 290
  ctx.clearRect(0, 0, 400, 300)

  if (eggs.size < 5) {
    for (let i = 0; i < 5; i++) {
      let egg = new Egg(Math.random() * 400, -20 - 300 * Math.random())
      eggs.add(egg)
    }
  }
  for (let egg of eggs.values()) {
    egg.y += 1
    ctx.fillStyle = "red"
    ctx.beginPath()
    ctx.arc(egg.x, egg.y, 10, 0, Math.PI * 2, false)
    ctx.closePath()
    ctx.fill()
    if (egg.y > 320) {
      eggs.delete(egg)
    }
    if (egg.y >= y - HEIGHT && egg.y <= y && egg.x >= x - WIDTH - HEIGHT && egg.x <= x + WIDTH + HEIGHT) {
      eggsCaught++
      score.innerHTML = `${eggsCaught} points`
      eggs.delete(egg)
    }
  }

  ctx.fillStyle = "orange"
  ctx.beginPath()
  ctx.arc(x - WIDTH, y - HEIGHT, HEIGHT, Math.PI, 1 / 2 * Math.PI, true)
  ctx.lineTo(x + WIDTH, y)
  ctx.arc(x + WIDTH, y - HEIGHT, HEIGHT, 1 / 2 * Math.PI, 0, true)
  ctx.closePath()
  ctx.fill()
  requestAnimationFrame(frame)
}

frame()
